/**
 * Import des classes
 */
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3200;

var salons = io.sockets.adapter.rooms;
/**
 * Affichage du fichier HTML (index) pou suive les données échangées
 */
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
/**
 * Réception et émission de messages vers WebSocket
 */
io.on('connection', (socket) => {
    socket.on('startingpoint', () => {
        io.emit('message', message);
    });
    socket.on('message', (message) => {

    });
    // Réception de l'ID de la carte
    socket.on('carteChange', (uid) => {
        console.log("Nouvelle carte ajoutée", uid);
        io.emit('carteChange', uid);
    });
    // La carte vient d'être enlevée
    socket.on('carteEnlevee', (uid) => {
        console.log("Une carte a été enlevée", uid);
        io.emit('carteEnlevee', uid);
    });
    // Forcer la réinitialisation de la carte côté NFC
    socket.on('initCarte', () => {
        console.log("Carte initialisée");
        io.emit('initUID');
    });
    /**
     * Recevoir le lancement d'un jeu et le broadcaster à l'ensemble des utilisateurs
     */
    socket.on('jeu', (jeu, startingpoint) => {
        io.to(jeu).emit('jeuDemarre', startingpoint);

    });
});
/**
 * Ecouter le serveur sur le port 3000
 */
server.listen(port, function () {
    console.log('Ecoute sur le port *:' + port);
});